<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

trait ApiResponse
{

    /**
     * Send success response.
     *
     * @param string         $message
     * @param array|JsonResource $data
     * @param int            $status
     *
     * @return JsonResponse
     */
    function success(string $message, $data, int $status = 200): JsonResponse
    {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
        ], $status);
    }

    /**
    * send error response
    * @param  string $message
    * @param  array $errors
    * @param  int $staus
    * @return JsonResponse
    */
    function error(string $message, int $status = 400, array $errors = null) :JsonResponse
    {
        return response()->json([
            "status"  => false,
            "message" => $message,
            "errors"  => $errors ? $errors :[$message]
        ], $status);
    }

}
