<?php

namespace App\Services;

use App\Models\Article;

class ArticleService
{
    public function read()
    {
        return Article::paginate(20);
    }

    public function create(array $data)
    {
        return Article::create($data);
    }

    public function update(Article $article, array $data)
    {
        $article->update($data);

        return $article;
    }

    public function delete(Article $article)
    {
        $article->delete();
    }
}