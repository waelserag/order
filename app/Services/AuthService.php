<?php

namespace App\Services;

use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    use ApiResponse;

    /**
     * Register a new user.
     *
     * @param array $data
     * @return User
     */
    public function registerUser(array $data): User
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return $user;
    }

    /**
     * Log in a user and generate a JWT token.
     *
     * @param array $data
     * @return array
     * @throws ValidationException
     */
    public function loginUser(array $data): array
    {
        $credentials = Arr::only($data, ['email', 'password']);

        if (!Auth::attempt($credentials)) {
            throw new HttpResponseException($this->error("Invalid email or password", 422));
        }

        $user = Auth::user();
        $user->token = JWTAuth::fromUser($user);

        return collect($user)->toArray();
    }
}