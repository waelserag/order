<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Services\ArticleService;

class ArticleController extends Controller
{
    public function __construct(protected ArticleService $articleService){}

    public function index()
    {
        $articles = $this->articleService->read();

        return ArticleResource::collection($articles);
    }

    public function store(ArticleRequest $request)
    {
        $article = $this->articleService->create($request->validated());

        $articleResource = new ArticleResource($article);
        return $this->success("Articles", $articleResource);

    }

    public function show(Article $article)
    {
        $articleResource = new ArticleResource($article);
        return $this->success("Articles", $articleResource);
    }

    public function update(ArticleRequest $request, Article $article)
    {
        $updatedArticle = $this->articleService->update($article, $request->validated());

        $articleResource = new ArticleResource($updatedArticle);
        return $this->success("Articles", $articleResource);
    }

    public function destroy(Article $article)
    {
        $this->articleService->delete($article);

        return response()->noContent();
    }
}
