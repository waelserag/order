<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Services\AuthService;

class LoginController extends Controller
{
    public function __construct(protected AuthService $authService){}

    public function store(LoginRequest $request)
    {
        $user = $this->authService->loginUser($request->validated());

        return $this->success("Logged in successful", $user, 201);
    }
}
