<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Services\AuthService;

class RegisterController extends Controller
{
    public function __construct(protected AuthService $authService){}

    public function store(RegisterRequest $request)
    {
        $user = $this->authService->registerUser($request->validated());

        $data = new UserResource($user);
        return $this->success("Registration successful", $data, 201);
    }
}
