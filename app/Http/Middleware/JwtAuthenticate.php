<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class JwtAuthenticate
{
    use ApiResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            // Check the token
            $user = JWTAuth::parseToken()->authenticate();
            if (!$user) {
                return $this->error('User not found', 404);
            }
        } catch (JWTException $e) {
            // Handle errors like token expired, token invalid
            return $this->error($e->getMessage(), 401);
        }

        // If authentication is successful, proceed with the request
        return $next($request);
    }
}
