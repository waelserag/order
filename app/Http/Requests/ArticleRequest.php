<?php

namespace App\Http\Requests;

use App\Traits\ApiResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ArticleRequest extends FormRequest
{
    use ApiResponse;

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException($this->error("Validation Error", 400, $validator->messages()->all()));
    }

    public function rules()
    {
        return [
            'title'   => 'required|string|max:255',
            'content' => 'required|string',
        ];
    }
}